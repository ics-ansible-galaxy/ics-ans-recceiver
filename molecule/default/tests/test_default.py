import os
from ast import literal_eval
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('recceiver')


def test_container(host):
    with host.sudo():
        cmd = host.run('docker ps')
    assert cmd.rc == 0
    names = sorted([line.split()[-1] for line in cmd.stdout.strip().split('\n')[1:]])
    assert names == [u'recceiver']


def test_broadcast(host):
    cmd = host.run('python -c \'import sys,socket; s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0); s.settimeout(15); s.bind(("",5049)); print(s.recvfrom(1024));\'')
    assert cmd.rc == 0
    (message, (ip, port)) = literal_eval(cmd.stdout)
    assert ip in host.interface("eth0").addresses
    assert host.socket("udp://0.0.0.0:%d" % port).is_listening
